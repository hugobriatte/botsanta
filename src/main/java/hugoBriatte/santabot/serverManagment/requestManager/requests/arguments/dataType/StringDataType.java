package hugoBriatte.santabot.serverManagment.requestManager.requests.arguments.dataType;

import java.util.InputMismatchException;

import org.javacord.api.entity.server.Server;

public class StringDataType implements DataType<String> {

	@Override
	public String translate(Server serv, String input) throws InputMismatchException {
		return input;
	}

}
