package hugoBriatte.santabot.serverManagment.requestManager.requests.arguments.dataType;

import java.util.Collection;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Map.Entry;

import org.javacord.api.entity.server.Server;

import hugoBriatte.santabot.serverManagment.requestManager.requests.Request;

public class CommandDataType implements DataType<Request> {
	
	protected Map<String, Request> requests;

	public CommandDataType(Map<String, Request> requests) {
		super();
		this.requests = requests;
	}



	@Override
	public Request translate(Server serv, String input) throws InputMismatchException {
		for(Entry<String, Request> entry : requests.entrySet()) {
			if(entry.getKey().equals(input)) return entry.getValue();
		}
		throw new InputMismatchException();
	}

}
