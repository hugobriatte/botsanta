package hugoBriatte.santabot.serverManagment.requestManager.requests;

import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.event.message.MessageCreateEvent;

import hugoBriatte.santabot.serverManagment.dataManagment.DataQuery;
import hugoBriatte.santabot.serverManagment.dataManagment.exception.AllreadyPresentException;
import hugoBriatte.santabot.serverManagment.dataManagment.exception.DataQueryException;

public class RemoveRequest extends Request {
	
	protected DataQuery query;

	public RemoveRequest(DataQuery query) {
		super("Enlève une personne des participants du secret santa");
		this.query = query;
	}

	@Override
	public void execute(String argumentList, MessageCreateEvent event) {
		MessageBuilder response = new MessageBuilder();
		try {
			query.removeUser(event.getMessage().getAuthor().asUser().get(), event.getServer().get());
			response.append("Vous avez bien été enlevé du secret santa de ce serveur");
		} catch (DataQueryException e) {
			response.append(BDDERROR);
		}
		response.send(event.getChannel());
	}

}
