package hugoBriatte.santabot.serverManagment.dataManagment;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;

import hugoBriatte.santabot.serverManagment.dataManagment.exception.AllreadyPresentException;
import hugoBriatte.santabot.serverManagment.dataManagment.exception.DataQueryException;

/**
 * Interface for strategies of dataManagment
 * DataBase for the moment, could be implemented by other strategoes
 */
public interface DataQuery {
	
	/**
	 * Add a user to the data
	 * @param user The user
	 * @throws DataQueryException if something went wrong
	 */
	public void addUser(User user, Server serv) throws DataQueryException, AllreadyPresentException;
	
	/**
	 * Add a server to the data
	 * @param server The server
	 * @throws DataQueryException if something went wrong
	 */
	public void addServer(Server server) throws DataQueryException, AllreadyPresentException;
	
	/**
	 * Add a link to the data
	 * @param userGiver The giver
	 * @param userReciver The reciver
	 * @param server The server
	 * @throws DataQueryException if something went wrong
	 */
	public void addLink(User userGiver, User userReciver, Server server) throws DataQueryException, AllreadyPresentException;
	
	/**
	 * Get the giver
	 * @param reviver The reciver
	 * @param server The server
	 * @throws DataQueryException If something went wrong
	 */
	public User getGiver(User reviver, Server server) throws DataQueryException;
	
	/**
	 * Get the reciver of someone
	 * @param reciver The reciver
	 * @param server The server
	 * @throws DataQueryException If something went wrong
	 */
	public User getReciver(User reciver, Server server) throws DataQueryException;
	
	/**
	 * Check if a user is participating
	 * @param user The user
	 * @return true if he participate, false otherwise
	 * @throws DataQueryException If something went wrong
	 */
	public boolean isParticipating(User user, Server server) throws DataQueryException;
	
	/**
	 * Check if a server is participating
	 * @param server The server
	 * @return true if he participate, false otherwise
	 * @throws DataQueryException If something went wrong
	 */
	public boolean isParticipating(Server server) throws DataQueryException;
	
	/**
	 * Remove a server from the database
	 * @param server The serveur
	 * @throws DataQueryException If something went wrong
	 */
	public void removeServer(Server server) throws DataQueryException;
	
	/**
	 * Remove a user on a server from the database
	 * @param user The user
	 * @param serv The server
	 * @throws DataQueryException If something went wrong
	 */
	public void removeUser(User user, Server serv) throws DataQueryException;
	
	/**
	 * Get all participantes from a secret santa
	 * @param serv The server
	 * @return A list of user participating
	 * @throws DataQueryException If something went wrong
	 */
	public List<User> getParticipants(Server serv) throws DataQueryException;
	
	/**
	 * Remove all giver <-> reciver links from the data
	 * @param serv The server
	 * @throws DataQueryException Is something went wrong
	 */
	public void resetLinks(Server serv) throws DataQueryException;
	
	/**
	 * Return if the secret santa has started for a server
	 * @param serv The server
	 * @return true if it has started, false otherwise
	 * @throws DataQueryException If something went wrong
	 */
	public boolean hasBeenStarted(Server serv) throws DataQueryException;
	
	public void setStarted(Server serv, boolean value) throws DataQueryException;
	
	public Timestamp getEndDate(Server serv) throws DataQueryException;
	
	public void setEndDate(Server serv, Timestamp date) throws DataQueryException;
}
