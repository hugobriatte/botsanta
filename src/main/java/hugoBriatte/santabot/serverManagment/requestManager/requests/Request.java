package hugoBriatte.santabot.serverManagment.requestManager.requests;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Collection;
import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.javacord.api.entity.server.Server;
import org.javacord.api.event.message.MessageCreateEvent;

import hugoBriatte.santabot.serverManagment.requestManager.requests.arguments.ArgumentDescriptor;
import hugoBriatte.santabot.serverManagment.requestManager.requests.arguments.ArgumentsMap;
import hugoBriatte.santabot.serverManagment.requestManager.requests.arguments.dataType.DataType;

import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.AbstractMap.SimpleEntry;

/**
 * Class that define a request for the User
 * @author Hugo
 *
 */
public abstract class Request {
	
	protected ArgumentsMap<DataType<?>> argumentsExcepted;
	protected String description;
	public static final String BDDERROR = "Snif snif, ca marche poa :c\n"
			+ "Ce n'est pas votre faute mais celle du dev, donc hesitez pas a le spam";
	
	public Request(ArgumentDescriptor[] names, DataType<?>[] dataTypes, String description) {
		this.argumentsExcepted = new ArgumentsMap<DataType<?>>();
		for(int i = 0; i < names.length; i += 1) {
			argumentsExcepted.put(names[i], dataTypes[i]);
		}
		this.description = description;
	}
	
	public Request(String description) {
		this(new ArgumentDescriptor[] {}, new DataType<?>[] {}, description);
	}
	
	public class ParseRequestResult {
		public String left;
		public ArgumentsMap<Entry<String, Object>> arg;
		
		public ParseRequestResult(String left, ArgumentsMap<Entry<String, Object>> values) {
			super();
			this.left = left;
			this.arg = values;
		}
		
		
	}
	
	public ParseRequestResult translate(Server serv, String argumentList) throws InputMismatchException {
		ArgumentsMap<Entry<String, Object>> explicitArg = new ArgumentsMap<Entry<String, Object>>();
		
		try(Scanner scanner = new Scanner(argumentList)) {
			
			/* Parcour pour les arguments explicite */
			String input = scanner.next();
			while(input.charAt(0) == '-') {
				if(input.length() < 2) throw new InputMismatchException();
				else {
					Entry<ArgumentDescriptor, DataType<?>> entry = argumentsExcepted.getEntry(input.charAt(1) != '-', input.replaceAll("-", ""));
					try {
						input = scanner.next();
						if(entry.getValue() != null) explicitArg.put(entry.getKey(), new AbstractMap.SimpleEntry<String, Object>(input, entry.getValue().translate(serv, input)));
						else explicitArg.put(entry.getKey(), new AbstractMap.SimpleEntry<String, Object>(input, true));
					} catch (NoSuchElementException e) {
						throw new InputMismatchException();
					}
				}
				try {
					input = scanner.next();
				} catch (NoSuchElementException e) {
					System.out.println("BLEU");
				}
			}
			
			String left = "";
			
			/* Parcour pour les arguments restant */
			do {
				for(Entry<ArgumentDescriptor, DataType<?>> arg : argumentsExcepted.entrySet()) {
					if(!explicitArg.containsKey(arg.getKey())
							&& arg.getKey().canBeAnonymous()) {
						try {
							explicitArg.put(arg.getKey(), new AbstractMap.SimpleEntry<String, Object>(input, arg.getValue().translate(serv, input)));
							break;
						} catch (InputMismatchException e) {
							System.out.println("DAB");
						}
					}
				}
				left += input;
				try {
					input = scanner.next();
				} catch (Exception e) {
					System.out.println("LA");
				}
			} while(scanner.hasNext());
			
			/* Verif final */
			for(Entry<ArgumentDescriptor, DataType<?>> arg : argumentsExcepted.entrySet()) {
				if(arg.getKey().isObligatory()
						&& !explicitArg.containsKey(arg.getKey())) {
					throw new InputMismatchException();
				}
			}
			
			scanner.useDelimiter("");
			try {
				left += scanner.next();
			} catch(NoSuchElementException e) {
				System.out.println("ICI");
			}
			return new ParseRequestResult(left, explicitArg);
		}catch (InputMismatchException e) {
			throw e;
		} catch (NoSuchElementException e) {
			return new ParseRequestResult("", new ArgumentsMap<Map.Entry<String,Object>>());
		}
	}
	
	public abstract void execute(String argumentList, MessageCreateEvent event);
}
