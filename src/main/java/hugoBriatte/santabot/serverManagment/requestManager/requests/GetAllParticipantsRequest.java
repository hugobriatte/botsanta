package hugoBriatte.santabot.serverManagment.requestManager.requests;

import java.util.List;

import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;

import hugoBriatte.santabot.serverManagment.dataManagment.DataQuery;
import hugoBriatte.santabot.serverManagment.dataManagment.exception.DataQueryException;

public class GetAllParticipantsRequest extends Request {
	
	protected DataQuery query;

	public GetAllParticipantsRequest(DataQuery query) {
		super("Retourne la liste les participants");
		this.query = query;
	}

	@Override
	public void execute(String argumentList, MessageCreateEvent event) {
		MessageBuilder builder = new MessageBuilder();
		try {
			List<User> participants = query.getParticipants(event.getServer().get());
			builder.append("Voici les participants actuels du secret santa UwU\n");
			for(User u : participants) {
				builder.append(u.getName() + "\n");
			}
		} catch (DataQueryException e) {
			builder.append(BDDERROR);
		}
		builder.send(event.getChannel());
	}

}
