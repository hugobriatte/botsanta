package hugoBriatte.santabot.serverManagment.dataManagment.dataBase;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import hugoBriatte.santabot.serverManagment.dataManagment.dataBase.DataBaseQuery;
import hugoBriatte.santabot.serverManagment.dataManagment.exception.AllreadyPresentException;
import hugoBriatte.santabot.serverManagment.dataManagment.exception.DataQueryException;

public class DataBaseQueryTest {
	
	static Connection con;
	static DataBaseQuery query;
	
	@BeforeClass
	public static void beforeClass() {
		try {
			query = new DataBaseQuery(DataBaseTestUtil.URL, DataBaseTestUtil.USER, DataBaseTestUtil.PASSWD);
			con = query.getConnection();
		} catch (SQLException e1) {
			fail();
		}
	}
	
	@Before
	public void before() {
		try {
			query.resetTable();;
		} catch(SQLException e) {
			fail();
		}
	}
	
	@Test
	public void testAddUser() {
		try {
			query.addServer(5);
			query.addUser(12, 5);
			ResultSet set = con.prepareStatement("SELECT * FROM santabot_user").executeQuery();
			set.next();
			assertTrue(set.getLong(1) == 12);
			assertTrue(set.isLast());
			try {
				query.addUser(12, 5);
				fail();
			} catch (AllreadyPresentException e) {
				/* Bonne erreur */
			}
		} catch (DataQueryException | SQLException e) {
			fail();
		}
	}
	
	@Test
	public void testAddServer() {
		try {
			query.addServer(7);
			ResultSet set = con.prepareStatement("SELECT * FROM santabot_server").executeQuery();
			set.next();
			assertTrue(set.getLong(1) == 7);
			assertTrue(set.isLast());
			try {
				query.addServer(7);
				fail();
			} catch (AllreadyPresentException e) {
				/* Bonne erreur */
			}
		} catch (DataQueryException | SQLException e) {
			fail();
		}
	}
	
	@Test
	public void testAddLink() {
		try {
			query.addServer(9);
			query.addUser(2, 9);
			query.addUser(7, 9);
			query.addLink(2, 7, 9);
			ResultSet set = con.prepareStatement("SELECT IDgiver, IDreciever, IDserver FROM santabot_links").executeQuery();
			set.next();
			assertTrue(set.getLong("IDgiver") == 2);
			assertTrue(set.getLong("IDreciever") == 7);
			assertTrue(set.getLong("IDserver") == 9);
		}catch (DataQueryException | SQLException e) {
			
		}
		try {
			query.addLink(2, 7, 9);
			fail();
		} catch (DataQueryException e) {
			
		}
	}
	
	@Test
	public void testGetGiverId() {
		try {
			query.addServer(9);
			query.addUser(2, 9);
			query.addUser(7, 9);
			query.addUser(8, 9);
			query.addLink(2, 7, 9);
			query.addLink(8, 2, 9);
			assertTrue(query.getGiverId(7, 9) == 2);
			assertTrue(query.getGiverId(2, 9) == 8);
		} catch(DataQueryException e) {
			fail();
		}
	}
	
	@Test
	public void testGetReciverId() {
		try {
			query.addServer(9);
			query.addUser(2, 9);
			query.addUser(7, 9);
			query.addUser(8, 9);
			query.addLink(2, 7, 9);
			query.addLink(8, 2, 9);
			assertTrue(query.getReciverId(2, 9) == 7);
			assertTrue(query.getReciverId(8, 9) == 2);
		} catch(DataQueryException e) {
			fail();
		}
	}
	
	@Test
	public void isServerParticipating() {
		try {
			query.addServer(9);
			assertTrue(query.isServerParticipating(9));
			assertFalse(query.isServerParticipating(157));
		} catch(DataQueryException e) {
			fail();
		}
	}
	
	@Test
	public void isUserParticipating() {
		try {
			query.addServer(9);
			query.addUser(2, 9);
			query.addUser(7, 9);
			query.addUser(8, 9);
			assertTrue(query.isUserParticipating(2, 9));
			assertTrue(query.isUserParticipating(7, 9));
		} catch(DataQueryException e) {
			fail();
		}
	}
}
