package hugoBriatte.santabot;

import java.sql.SQLException;
import java.util.Arrays;

import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.intent.Intent;

import hugoBriatte.santabot.serverManagment.BotManager;
import hugoBriatte.santabot.serverManagment.dataManagment.DataQuery;
import hugoBriatte.santabot.serverManagment.dataManagment.dataBase.DataBaseQuery;

public class App {

	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		DiscordApi api = new DiscordApiBuilder().setToken(args[0])
				.setAllIntentsExcept(Intent.GUILD_BANS, Intent.GUILD_EMOJIS,
						Intent.GUILD_VOICE_STATES, Intent.DIRECT_MESSAGE_TYPING,
						Intent.DIRECT_MESSAGE_REACTIONS,
						Intent.GUILD_MESSAGE_REACTIONS,
						Intent.GUILD_MESSAGE_TYPING).login().join();
		Class.forName("org.h2.Driver");
		System.out.println(Arrays.deepToString(args));
		DataQuery query = new DataBaseQuery("jdbc:h2:" + args[1], "sa", "");
		BotManager manager = new BotManager(api, query);
	}

}
