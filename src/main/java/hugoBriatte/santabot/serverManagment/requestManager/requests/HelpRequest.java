package hugoBriatte.santabot.serverManagment.requestManager.requests;

import java.util.Collection;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Map.Entry;

import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.event.message.MessageCreateEvent;

import hugoBriatte.santabot.serverManagment.requestManager.requests.arguments.ArgumentDescriptor;
import hugoBriatte.santabot.serverManagment.requestManager.requests.arguments.dataType.CommandDataType;
import hugoBriatte.santabot.serverManagment.requestManager.requests.arguments.dataType.DataType;

public class HelpRequest extends Request {
	
	protected static final String dESCRIPTION = "Affiche l'aide utilisateur";
	protected Map<String, Request> requests;

	public HelpRequest(Map<String, Request> requests) {
		super(new ArgumentDescriptor[] {new ArgumentDescriptor("command", "c", "La commande sur laquelle on veut de l'aide", false, true)},
				new DataType<?>[] {new CommandDataType(requests)},
				"Affiche l'aide utilisateur");
		this.requests = requests;
	}
	
	

	@Override
	public void execute(String argumentList, MessageCreateEvent event) {
		try {
			EmbedBuilder builder = new EmbedBuilder();
			ParseRequestResult res = translate(event.getServer().get(), argumentList);
			Entry<ArgumentDescriptor, Entry<String, Object>> entry = res.arg.getEntry(true, "c");
			if(entry != null) {
				Entry<String, Object> userInput = entry.getValue();
				builder.setTitle(userInput.getKey());
				Request request = ((Request) userInput.getValue());
				builder.setDescription(request.description);
				for(ArgumentDescriptor arg : request.argumentsExcepted.keySet()) {
					builder.addField(arg.getBigName() + " (-" + arg.getLittleName() + ") Obligatoire : " + (arg.isObligatory() ? "Oui" : "Non"), arg.getDescription());
				}
			} else {
				builder.setTitle("Les commandes :3");
				for(Entry<String, Request> req : requests.entrySet()) {
					StringBuilder keyBuilder = new StringBuilder();
					keyBuilder.append(req.getKey());
					keyBuilder.append(" ");
					for(ArgumentDescriptor name : req.getValue().argumentsExcepted.keySet()) {
						keyBuilder.append("[-");
						keyBuilder.append(name.getLittleName());
						if(name.isObligatory()) keyBuilder.append("]");
						keyBuilder.append(" ");
						keyBuilder.append(name.getBigName());
						if(!name.isObligatory()) keyBuilder.append("]");
						keyBuilder.append(" ");
					}
					builder.addField(keyBuilder.toString(), req.getValue().description);
				}
			}
			event.getChannel().sendMessage(builder);
		} catch (InputMismatchException e) {
			event.getChannel().sendMessage("La commande à été mal écrite");
		}
	}

}
