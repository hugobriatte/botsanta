package hugoBriatte.santabot.serverManagment.dataManagment.dataBase;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;

import hugoBriatte.santabot.serverManagment.dataManagment.DataQuery;
import hugoBriatte.santabot.serverManagment.dataManagment.exception.AllreadyPresentException;
import hugoBriatte.santabot.serverManagment.dataManagment.exception.DataQueryException;

public class DataBaseQuery implements DataQuery {
	
	private String database;
	private String user;
	private String passwd;
	
	public DataBaseQuery(String database, String user, String passwd) throws SQLException {
		this.database = database;
		this.user = user;
		this.passwd = passwd;
		try {
			instantiateTable();
		} catch (Exception e) {
			System.out.println("Lol");
		}
	}
	
	protected Connection getConnection() throws SQLException {
		return DriverManager.getConnection(database, user, passwd);
	}

	@Override
	public void addUser(User user, Server serv) throws DataQueryException {
		try {
			addServer(serv);
		} catch(AllreadyPresentException e) {
			
		}
		addUser(user.getId(), serv.getId());
	}
	
	/**
	 * Add an id to the user table
	 * @param id the idTable
	 * @throws DataQueryException if something went wrong
	 */
	protected void addUser(long user, long server) throws DataQueryException {
		if(!isUserParticipating(user, server)) {
			Connection con = null;
			try {
				String query = "INSERT INTO santabot_user VALUES(?, ?)";
				con = getConnection();
				PreparedStatement ps = con.prepareStatement(query);
				ps.setLong(1, user);
				ps.setLong(2, server);
				ps.executeUpdate();
				con.close();
			} catch (SQLException e) {
				try {
					con.close();
				} catch (SQLException | NullPointerException e1) {	}
				System.out.println(e);
				throw new DataQueryException("La valeur n'a pas pu être rentré");
			}
		} else throw new AllreadyPresentException();
	}

	@Override
	public void addServer(Server server) throws DataQueryException {
		addServer(server.getId());
	}

	/**
	 * Add an id to the table server
	 * @param id The id of the table
	 * @throws DataQueryException if something went wrong
	 */
	protected void addServer(long id) throws DataQueryException {
		if(!isServerParticipating(id)) {
			Connection con = null;
			LocalDate today = LocalDate.now();
			int year;
			if(today.getMonthValue() < 12 || today.getDayOfMonth() < 25) year = today.getYear();
			else year = today.getYear() + 1;
			try {
				String query = "INSERT INTO santabot_server(ID, ENDDATE) VALUES(?, ?)";
				con = getConnection();
				PreparedStatement ps = con.prepareStatement(query);
				ps.setLong(1, id);
				ps.setTimestamp(2, Timestamp.valueOf(LocalDateTime.of(year, 12, 25, 0, 0)));
				ps.executeUpdate();
				con.close();
			} catch (SQLException e) {
				try {
					con.close();
				} catch (SQLException | NullPointerException e1) {	}
				throw new DataQueryException("La valeur n'a pas pu être rentré");
			}
		} else throw new AllreadyPresentException();
	}

	@Override
	public void addLink(User userGiver, User userReciver, Server server) throws DataQueryException {
		addLink(userGiver.getId(), userReciver.getId(), server.getId());
	}

	/**
	 * Add a link to the linkTable
	 * @param userGiver id of giver
	 * @param userReciver id of reciever
	 * @param server id of server
	 * @throws DataQueryException if something went wrong
	 */
	protected void addLink(long userGiver, long userReciver, long server) throws DataQueryException {
		Connection con = null;
		try {
			String query = "INSERT INTO santabot_links VALUES(?, ?, ?)";
			con = getConnection();
			PreparedStatement ps = con.prepareStatement(query);
			ps.setLong(1, server);
			ps.setLong(2, userGiver);
			ps.setLong(3, userReciver);
			ps.executeUpdate();
			con.close();
		} catch (SQLException e) {
			try {
				con.close();
			} catch (SQLException | NullPointerException e1) {	}
			throw new DataQueryException("La valeur n'a pas pu être rentré");
		}
	}

	@Override
	public User getGiver(User reviver, Server server) throws DataQueryException {
		return server.getMemberById(getGiverId(reviver.getId(), server.getId())).orElse(null);
	}
	
	/**
	 * Get the giver id of a link
	 * @param reviver The reciver id
	 * @param server The server id
	 * @return the giver id
	 * @throws DataQueryException if something went wrong
	 */
	protected long getGiverId(long reviver, long server) throws DataQueryException {
		Connection con = null;
		try {
			String query = "SELECT IDgiver FROM santabot_links WHERE IDreciever = ? AND IDserver = ?";
			con = getConnection();
			PreparedStatement ps = con.prepareStatement(query);
			ps.setLong(1, reviver);
			ps.setLong(2, server);
			ResultSet set = ps.executeQuery();
			set.next();
			long res = set.getLong("IDgiver");
			con.close();
			return res;
		} catch (SQLException e) {
			try {
				con.close();
			} catch(SQLException | NullPointerException e1) {	}
			throw new DataQueryException("La valeur n'a pas pu être recherché");
		}
	}

	@Override
	public User getReciver(User giver, Server server) throws DataQueryException {
		return server.getMemberById(getReciverId(giver.getId(), server.getId())).orElse(null);
	}

	/**
	 * Get the reciver id of a link
	 * @param giver The giver id
	 * @param server The server id
	 * @return the reciver id
	 * @throws DataQueryException if something went wrong
	 */
	protected long getReciverId(long giver, long server) throws DataQueryException {
		Connection con = null;
		try {
			String query = "SELECT IDreciever FROM santabot_links WHERE IDgiver = ? AND IDserver = ?";
			con = getConnection();
			PreparedStatement ps = con.prepareStatement(query);
			ps.setLong(1, giver);
			ps.setLong(2, server);
			ResultSet set = ps.executeQuery();
			set.next();
			long res = set.getLong("IDreciever");
			con.close();
			return res;
		} catch (SQLException e) {
			try {
				con.close();
			} catch(SQLException | NullPointerException e1) {	}
			throw new DataQueryException("La valeur n'a pas pu être recherché");
		}
	}

	@Override
	public boolean isParticipating(User user, Server server) throws DataQueryException {
		return isUserParticipating(user.getId(), server.getId());
	}
	
	/**
	 * Check if a user is participating
	 * @param user the user id
	 * @param server the server id
	 * @return if he's participating in this server
	 * @throws DataQueryException
	 */
	protected boolean isUserParticipating(long user, long server) throws DataQueryException {
		if(!isServerParticipating(server)) return false;
		Connection con = null;
		try {
			String query = "SELECT COUNT(*) FROM santabot_user WHERE ID_server = ? AND ID = ?";
			con = getConnection();
			PreparedStatement ps = con.prepareStatement(query);
			ps.setLong(1, server);
			ps.setLong(2, user);
			ResultSet set = ps.executeQuery();
			set.next();
			long res = set.getInt(1);
			con.close();
			return res > 0;
		} catch (SQLException e) {
			System.out.println(e);
			try {
				con.close();
			} catch(SQLException | NullPointerException e1) {	}
			throw new DataQueryException("La valeur n'a pas pu être recherché");
		}
	}

	@Override
	public boolean isParticipating(Server server) throws DataQueryException {
		return isServerParticipating(server.getId());
	}
	

	/**
	 * Check if a server is participating
	 * @param server the server
	 * @return true if he participate
	 * @throws DataQueryException
	 */
	protected boolean isServerParticipating(long server) throws DataQueryException {
		Connection con = null;
		try {
			String query = "SELECT COUNT(*) FROM santabot_server WHERE ID = ?";
			con = getConnection();
			PreparedStatement ps = con.prepareStatement(query);
			ps.setLong(1, server);
			ResultSet set = ps.executeQuery();
			set.next();
			long res = set.getInt(1);
			con.close();
			return res > 0;
		} catch (SQLException e) {
			System.out.println(e);
			try {
				con.close();
			} catch(SQLException | NullPointerException e1) {	}
			throw new DataQueryException("La valeur n'a pas pu être recherché");
		}
	}
	
	/**
	 * Create all the table needed
	 * @param con The connection
	 * @throws SQLException 
	 */
	protected void instantiateTable() throws SQLException {
		Connection con = getConnection();
		try {
			instantiateServerTable(con);
			instantiateUserTable(con);
			instantiateLinkTable(con);
			con.close();
		} catch (SQLException e) {
		    System.out.println(e);
			con.close();
			throw e;
		}
	}
	
	/**
	 * Create all the table needed
	 * @param con The connection
	 * @throws SQLException 
	 */
	protected void resetTable() throws SQLException {
		Connection con = getConnection();
		try {
			con.prepareStatement("DROP TABLE santabot_server, santabot_user, santabot_links IF EXISTS CASCADE").executeUpdate();
			con.close();
			instantiateTable();
		} catch (SQLException e) {
			con.close();
			throw e;
		}
	}

	/**
	 * Create the user table
	 * @param con The connection
	 * @throws SQLException 
	 */
	protected void instantiateUserTable(Connection con) throws SQLException {
		String query = "CREATE TABLE santabot_user(ID bigint, "
				+ "ID_server bigint, "
				+ "CONSTRAINT santabot_user_pk PRIMARY KEY(ID, ID_server), "
				+ "CONSTRAINT santabot_user_fk FOREIGN KEY(ID_server) REFERENCES santabot_server(ID) ON DELETE CASCADE)";
		con.prepareStatement(query).executeUpdate();
	}

	/**
	 * Create the user table
	 * @param con The connection
	 * @throws SQLException 
	 */
	protected void instantiateServerTable(Connection con) throws SQLException {
		String query = "CREATE TABLE santabot_server(ID bigint, "
				+ "EndDate TIMESTAMP,"
				+ "HasStarted BOOLEAN DEFAULT 'f',"
				+ "CONSTRAINT santabot_server_pk PRIMARY KEY(ID))";
		PreparedStatement ps = con.prepareStatement(query);
		ps.executeUpdate();
	}
	
	/**
	 * Create the link table
	 * @param con The connection
	 * @throws SQLException
	 */
	protected void instantiateLinkTable(Connection con) throws SQLException {
		String query = "CREATE TABLE santabot_links(IDserver bigint,"
				+ "IDgiver bigint, "
				+ "IDreciever bigint, "
				+ "CONSTRAINT santabot_link_pk PRIMARY KEY(IDserver, IDgiver), "
				+ "CONSTRAINT santabot_link_fk_IDgiver FOREIGN KEY(IDgiver, IDserver) REFERENCES santabot_user(ID, ID_server) ON DELETE CASCADE, "
				+ "CONSTRAINT santabot_link_fk_IDreciever FOREIGN KEY(IDreciever, IDserver) REFERENCES santabot_user(ID, ID_server) ON DELETE CASCADE)";
		con.prepareStatement(query).executeUpdate();
	}

	@Override
	public void removeServer(Server server) throws DataQueryException {
		removeServer(server.getId());
	}
	
	/**
	 * Remove a server in the database by id
	 * @param id The id
	 * @throws DataQueryException if something wentWrong
	 */
	protected void removeServer(long id) throws DataQueryException {
		Connection con = null;
		try {
			con = getConnection();
			String query = "DELETE FROM santabot_server WHERE ID = ?";
			PreparedStatement ps = con.prepareStatement(query);
			ps.setLong(1, id);
			ps.executeUpdate();
			con.close();
		} catch (SQLException e) {
			try {
				con.close();
			} catch (SQLException ee) {};
			throw new DataQueryException("Supression impossible");
		}
	}
	
	@Override
	public void removeUser(User user, Server serv) throws DataQueryException {
		removeUser(user.getId(), serv.getId());
	}

	protected void removeUser(long idU, long idS) throws DataQueryException {
		Connection con = null;
		try {
			con = getConnection();
			String query = "DELETE FROM santabot_user WHERE ID = ? AND ID_server = ?";
			PreparedStatement ps = con.prepareStatement(query);
			ps.setLong(1, idU);
			ps.setLong(2, idS);
			ps.executeUpdate();
			con.close();
		} catch (SQLException e) {
			try {
				con.close();
			} catch (SQLException ee) {};
			throw new DataQueryException("Supression impossible");
		}
	}

	@Override
	public List<User> getParticipants(Server serv) throws DataQueryException {
		List<User> res = new LinkedList<User>();
		for(long num : getParticipants(serv.getId())) {
			res.add(serv.getMemberById(num).get());
		}
		return res;
	}
	
	protected List<Long> getParticipants(long server)  throws DataQueryException {
		Connection con = null;
		try {
			con = getConnection();
			String query = "SELECT ID from santabot_user WHERE ID_server = ?";
			PreparedStatement ps = con.prepareStatement(query);
			ps.setLong(1, server);
			ResultSet set = ps.executeQuery();
			List<Long> res = new LinkedList<Long>();
			while(set.next()) {
				res.add(set.getLong(1));
			}
			con.close();
			return res;
		} catch (SQLException e) {
			try {
				con.close();
			} catch (SQLException ee) {};
			throw new DataQueryException("Supression impossible");
		}
	}

	@Override
	public void resetLinks(Server serv) throws DataQueryException {
		resetLinks(serv.getId());
	}
	
	protected void resetLinks(long serv) throws DataQueryException {
		Connection con = null;
		try {
			con = getConnection();
			String query = "DELETE FROM santabot_links WHERE IDserver = ?";
			PreparedStatement ps = con.prepareStatement(query);
			ps.setLong(1, serv);
			ps.executeUpdate();
			con.close();
		} catch (SQLException e) {
			try {
				con.close();
			} catch (SQLException ee) {};
			throw new DataQueryException("Supression impossible");
		}
	}

	@Override
	public boolean hasBeenStarted(Server serv) throws DataQueryException {
		return hasBeenStarted(serv.getId());
	}
	
	protected boolean hasBeenStarted(long serv) throws DataQueryException {
		Connection con = null;
		try {
			con = getConnection();
			String query = "SELECT HasStarted FROM santabot_server WHERE ID = ?";
			PreparedStatement ps = con.prepareStatement(query);
			ps.setLong(1, serv);
			ResultSet set = ps.executeQuery();
			set.next();
			boolean res = set.getBoolean(1);
			con.close();
			return res;
		} catch (SQLException e) {
			try {
				con.close();
			} catch (SQLException ee) {};
			throw new DataQueryException("Supression impossible");
		}
	}

	@Override
	public void setStarted(Server serv, boolean value) throws DataQueryException {
		setStarted(serv.getId(), value);
	}
	
	protected void setStarted(long serv, boolean value) throws DataQueryException {
		Connection con = null;
		try {
			con = getConnection();
			String query = "UPDATE santabot_server SET HasStarted = ? WHERE ID = ?";
			PreparedStatement ps = con.prepareStatement(query);
			ps.setBoolean(1, value);
			ps.setLong(2, serv);
			ps.executeUpdate();
			con.close();
		} catch (SQLException e) {
			try {
				con.close();
			} catch (SQLException ee) {};
			throw new DataQueryException("Supression impossible");
		}
	}

	@Override
	public Timestamp getEndDate(Server serv) throws DataQueryException {
		return getEndDate(serv.getId());
	}
	
	protected Timestamp getEndDate(long idServ) throws DataQueryException {
		Connection con = null;
		try {
			con = getConnection();
			String query = "SELECT EndDate FROM santabot_server WHERE ID = ?";
			PreparedStatement ps = con.prepareStatement(query);
			ps.setLong(1, idServ);
			ResultSet set = ps.executeQuery();
			set.next();
			Timestamp res = set.getTimestamp(1);
			con.close();
			return res;
		} catch (SQLException e) {
			try {
				con.close();
			} catch (SQLException ee) {};
			throw new DataQueryException("Supression impossible");
		}
	}

	@Override
	public void setEndDate(Server serv, Timestamp date) throws DataQueryException {
		setEndDate(serv.getId(), date);
	}
	
	protected void setEndDate(long idServ, Timestamp date) throws DataQueryException {
		Connection con = null;
		try {
			con = getConnection();
			String query = "UPDATE santabot_server SET EndDate = ? WHERE ID = ?";
			PreparedStatement ps = con.prepareStatement(query);
			ps.setTimestamp(1, date);
			ps.setLong(2, idServ);
			ps.executeUpdate();
			con.close();
		} catch (SQLException e) {
			try {
				con.close();
			} catch (SQLException ee) {};
			throw new DataQueryException("Supression impossible");
		}
	}

}
