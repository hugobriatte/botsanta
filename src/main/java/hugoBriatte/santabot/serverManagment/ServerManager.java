package hugoBriatte.santabot.serverManagment;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.javacord.api.entity.channel.ServerTextChannel;
import org.javacord.api.entity.permission.Role;
import org.javacord.api.entity.permission.RoleBuilder;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.server.ServerUpdater;
import org.javacord.api.entity.user.User;

public class ServerManager {
	
	protected ServerTextChannel defaultChannel;
	protected Role participantRole;
	public static final String ROLENAME = "sb_part";
	
	public ServerManager(Server serv) {
		int i = 0;
		List<ServerTextChannel> list = serv.getTextChannels();
		while(i < list.size() && !list.get(i).canYouWrite()) i += 1;
		defaultChannel = (i < list.size() ? list.get(i) : null);
		
		/* Research of a santabot role */
		List<Role> role = serv.getRolesByName(ROLENAME);
		if(!role.isEmpty()) {
			participantRole = role.get(0);
			ServerUpdater updater = new ServerUpdater(serv);
			for(User u : serv.getMembers()) {
				updater.removeRoleFromUser(u, participantRole);
			}
			try {
				updater.update().get();
			} catch (ExecutionException | InterruptedException e) {
				/* LOL */
			}
		}
		else {
			try {
				participantRole = new RoleBuilder(serv).setName(ROLENAME).create().get();
			} catch (Exception e) {
				System.out.println("Creation role impossible " + serv);
			}
		}
	}

	public ServerTextChannel getDefaultChannel() {
		return defaultChannel;
	}

	public void setDefaultChannel(ServerTextChannel defaultChannel) {
		this.defaultChannel = defaultChannel;
	}

	public Role getParticipantRole() {
		return participantRole;
	}
	
	
	
}
