package hugoBriatte.santabot.serverManagment.requestManager.requests.arguments.dataType;

import java.util.InputMismatchException;

import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.server.Server;

public class ChannelDataType implements DataType<TextChannel> {

	@Override
	public TextChannel translate(Server serv, String input) throws InputMismatchException {
		try {
			return serv.getTextChannelById(input).get();
		} catch (Exception e) {
			throw new InputMismatchException();
		}
	}

}
