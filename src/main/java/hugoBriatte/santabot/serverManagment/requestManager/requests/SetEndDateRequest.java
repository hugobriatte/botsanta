package hugoBriatte.santabot.serverManagment.requestManager.requests;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.InputMismatchException;

import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.event.message.MessageCreateEvent;

import hugoBriatte.santabot.serverManagment.dataManagment.DataQuery;
import hugoBriatte.santabot.serverManagment.dataManagment.exception.DataQueryException;
import hugoBriatte.santabot.serverManagment.requestManager.requests.arguments.ArgumentDescriptor;
import hugoBriatte.santabot.serverManagment.requestManager.requests.arguments.dataType.DataType;
import hugoBriatte.santabot.serverManagment.requestManager.requests.arguments.dataType.DateFormatter;

public class SetEndDateRequest extends Request {

	protected DataQuery query;
	
	public SetEndDateRequest(DataQuery query) {
		super(new ArgumentDescriptor[] {new ArgumentDescriptor("date", "d", "La date a laquelle l'évenement aura lieu sous la forme Année-Mois-Jour-Heure:Min", true, true)},
				new DataType<?>[] {new DateFormatter()},
				"Change la date de fin du secret santa du server");
		this.query = query;
	}

	@Override
	public void execute(String argumentList, MessageCreateEvent event) {
		MessageBuilder builder = new MessageBuilder();
		try {
			ParseRequestResult res = translate(event.getServer().get(), argumentList);
			query.setEndDate(event.getServer().get(), new Timestamp(((java.util.Date) res.arg.getEntry(true, "d").getValue().getValue()).getTime()));
			builder.append("C'est bon UwU");
		} catch (DataQueryException e) {
			builder.append(BDDERROR);
			
		} catch (InputMismatchException e) {
			builder.append("Je ne comprends pas " + argumentList + ". Il faut une date sous la forme Année-Mois-Jour-Heure:Min");
		}
		builder.send(event.getChannel());
	}

}
