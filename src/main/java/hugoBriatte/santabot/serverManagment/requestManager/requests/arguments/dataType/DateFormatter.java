package hugoBriatte.santabot.serverManagment.requestManager.requests.arguments.dataType;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.InputMismatchException;

import org.javacord.api.entity.server.Server;

public class DateFormatter implements DataType<Date> {
	
	public static final DateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd-HH:mm");

	@Override
	public Date translate(Server serv, String input) throws InputMismatchException {
		try {
			return FORMAT.parse(input);
		} catch (Exception e) {
			throw new InputMismatchException();
		}
	}

}
