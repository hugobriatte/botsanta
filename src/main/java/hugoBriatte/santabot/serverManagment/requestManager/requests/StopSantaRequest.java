package hugoBriatte.santabot.serverManagment.requestManager.requests;

import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;

import hugoBriatte.santabot.serverManagment.dataManagment.DataQuery;
import hugoBriatte.santabot.serverManagment.dataManagment.exception.DataQueryException;

public class StopSantaRequest extends Request {
	
	protected DataQuery query;

	public StopSantaRequest(DataQuery query) {
		super("Annule le secret santa");
		this.query = query;
	}

	@Override
	public void execute(String argumentList, MessageCreateEvent event) {
		MessageBuilder builder = new MessageBuilder();
		if(!event.getMessage().getAuthor().isServerAdmin()) {
			builder.append("Vous êtes poa admin :c");
		} else {
			try {
				query.removeServer(event.getServer().get());
				query.addServer(event.getServer().get());
				builder.append("Voili voilou");
			} catch (DataQueryException e ) {
				builder.append(BDDERROR);
			}
		}
		builder.send(event.getChannel());
	}

}
