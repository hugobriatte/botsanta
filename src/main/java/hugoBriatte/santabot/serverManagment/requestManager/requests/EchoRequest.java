package hugoBriatte.santabot.serverManagment.requestManager.requests;

import java.util.Map.Entry;

import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.event.message.MessageCreateEvent;

import hugoBriatte.santabot.serverManagment.requestManager.requests.arguments.ArgumentDescriptor;
import hugoBriatte.santabot.serverManagment.requestManager.requests.arguments.dataType.DataType;
import hugoBriatte.santabot.serverManagment.requestManager.requests.arguments.dataType.StringDataType;

public class EchoRequest extends Request {

	protected static final String dESCRIPTION = "Retourne le message envoyé par l'utilisateur";
	
	public EchoRequest() {
		super(new ArgumentDescriptor[] {new ArgumentDescriptor("signature", "s", "Finit la commande avec une signature", false, false),
				new ArgumentDescriptor("dab", "d", "Finit le message par un dab", false, false)},
				new DataType<?>[] {new StringDataType(), null},
				dESCRIPTION);
	}

	@Override
	public void execute(String argumentList, MessageCreateEvent event) {
		ParseRequestResult result = translate(event.getServer().get(), argumentList);
		Entry<ArgumentDescriptor, Entry<String, Object>> entry = result.arg.getEntry(true, "s");
		MessageBuilder builder = new MessageBuilder();
		builder.append(result.left);
		if(entry != null) builder.append("\n" + entry.getValue().getValue());
		if(result.arg.isDefined(true, "d")) builder.append("\nDab");
		builder.send(event.getChannel());
	}

}
