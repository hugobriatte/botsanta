package hugoBriatte.santabot.serverManagment.requestManager.requests;

import java.util.Date;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.server.ServerUpdater;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;

import hugoBriatte.santabot.serverManagment.ServerManager;
import hugoBriatte.santabot.serverManagment.dataManagment.DataQuery;
import hugoBriatte.santabot.serverManagment.dataManagment.exception.DataQueryException;

public class LaunchSantaRequest extends Request {

	protected DataQuery query;
	protected Map<Server, ServerManager> manager;
	protected static Timer timer = new Timer();
	protected static SimpleDateFormat dateFormat = new SimpleDateFormat("dd MM YYYY");
	
	public LaunchSantaRequest(DataQuery query, Map<Server, ServerManager> manager) {
		super("Lancer un Secret Santa");
		this.query = query;
		this.manager = manager;
		try {
			for(Server serv : manager.keySet())
				if(query.hasBeenStarted(serv))
					setTimer(serv);
		} catch (DataQueryException e) {
			/* RAS */
		}
	}

	@Override
	public void execute(String argumentList, MessageCreateEvent event) {
		MessageBuilder builder = new MessageBuilder();
		Server serv = event.getServer().get();
		ServerUpdater updater = new ServerUpdater(serv);
		try {
			if(!query.hasBeenStarted(serv)) {
				/* Ajout dans la base de donnée et calcul des relations */
				query.resetLinks(serv);
				List<User> participantsA = query.getParticipants(serv);
				Collections.shuffle(participantsA);
				for(int i = 0; i < participantsA.size(); i += 1) {
					User giver = participantsA.get(i);
					User reciver = participantsA.get((i + 1) % participantsA.size());
					
					query.addLink(giver, reciver, serv);
					giver.sendMessage("Bien le bonjour, dans le cas du secret santa du server " + serv.getName() + ", tu vas donner un cadeau a " + reciver.getName());
					/* Ajout du role a l'user */
					if(!giver.getRoles(serv).contains(manager.get(serv).getParticipantRole()))
						updater.addRoleToUser(giver, manager.get(serv).getParticipantRole());
				}
				
				/* Application des calculs */
				try {
					updater.update().get();
					query.setStarted(serv, true);
					
					/* Notification preparation */
					setTimer(serv);
					
					builder.append("Que le secret santa commence :3");
					
				} catch (InterruptedException | ExecutionException e) {
					builder = new MessageBuilder().append("Erreur lors de la mise du role");
				}
				
				/* Planif des taches */
			} else {
				builder.append("Le secret santa à déjà commencé");
			}
			
		} catch (DataQueryException e) {
			builder.append(BDDERROR);
		}
		builder.send(event.getChannel());
	}
	
	protected void setTimer(Server serv) throws DataQueryException {
		Date endDate = query.getEndDate(serv);
		Date now = Date.from(Instant.now());
		if(endDate.after(now)) {
			long nbSecondBetween = endDate.getTime() - now.getTime();
			for(int i = 1; i <= 4; i *= 2) {
				timer.schedule(new SendMessage(serv,"RAPPEL : Le secret santa se termine le " + dateFormat.format(endDate)), 
						new Date(endDate.getTime() - (nbSecondBetween * i) / 8));
			}
			timer.schedule(new StopSanta(serv,"Le secret santa est fini! Veuilez donner vos cadeaux immédiatement!"), 
					endDate);
		} else query.setStarted(serv, false);
	}
	
	public class SendMessage extends TimerTask {
		
		protected String message;
		protected Server serv;

		public SendMessage(Server serv, String message) {
			super();
			this.message = message;
			this.serv = serv;
		}

		@Override
		public void run() {
			manager.get(serv).getDefaultChannel().sendMessage(manager.get(serv).getParticipantRole().getMentionTag() + " " + message);
		}
		
	}
	
	public class StopSanta extends SendMessage {

		public StopSanta(Server serv, String message) {
			super(serv, message);
		}

		@Override
		public void run() {
			super.run();
			try {
				query.removeServer(serv);
				query.addServer(serv);
			} catch (DataQueryException e) {
				/* Lol */
			}
		}
		
		
		
	}
}
