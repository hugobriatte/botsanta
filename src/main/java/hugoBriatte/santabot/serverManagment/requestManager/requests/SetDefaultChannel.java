package hugoBriatte.santabot.serverManagment.requestManager.requests;

import java.util.List;
import java.util.Map;

import org.javacord.api.entity.channel.ServerTextChannel;
import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.entity.server.Server;
import org.javacord.api.event.message.MessageCreateEvent;

import hugoBriatte.santabot.serverManagment.ServerManager;
import hugoBriatte.santabot.serverManagment.requestManager.requests.arguments.ArgumentDescriptor;
import hugoBriatte.santabot.serverManagment.requestManager.requests.arguments.dataType.ChannelDataType;
import hugoBriatte.santabot.serverManagment.requestManager.requests.arguments.dataType.DataType;

public class SetDefaultChannel extends Request {
	
	protected Map<Server, ServerManager> manager;

	public SetDefaultChannel(Map<Server, ServerManager> manager) {
		super(new ArgumentDescriptor[] {new ArgumentDescriptor("channel", "c", "Le channel sur lequel les messages vont arriver", true, true)},
				new DataType<?>[] {new ChannelDataType()},
				"Set le channel par defaut");
		this.manager = manager;
	}

	@Override
	public void execute(String argumentList, MessageCreateEvent event) {
		MessageBuilder builder = new MessageBuilder();
		List<ServerTextChannel> channels = event.getMessage().getMentionedChannels();
		if(channels.isEmpty()) builder.append("Aucun channel mentionné :x");
		else {
			try {
				manager.get(event.getServer().get()).setDefaultChannel(event.getMessage().getMentionedChannels().get(0));
				builder.append("Channel changé");
			} catch (Exception e) {
				builder.append("Je n'ai pas pu réaliser votre demande :x");
			}
		}
		builder.send(event.getChannel());
	}

}
