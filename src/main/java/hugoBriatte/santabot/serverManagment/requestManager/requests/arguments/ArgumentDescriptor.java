package hugoBriatte.santabot.serverManagment.requestManager.requests.arguments;

public class ArgumentDescriptor {
	
	protected String bigName;
	protected String littleName;
	protected String description;
	protected boolean isObligatory;
	protected boolean canBeAnonymous;
	
	public ArgumentDescriptor(String bigName, String littleName, String description, boolean isObligatory, boolean canBeAnonymous) {
		super();
		this.bigName = bigName;
		this.littleName = littleName;
		this.description = description;
		this.isObligatory = isObligatory;
		this.canBeAnonymous = canBeAnonymous;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bigName == null) ? 0 : bigName.hashCode());
		result = prime * result + ((littleName == null) ? 0 : littleName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArgumentDescriptor other = (ArgumentDescriptor) obj;
		if (bigName == null) {
			if (other.bigName != null)
				return false;
		} else if (!bigName.equals(other.bigName))
			return false;
		if (littleName == null) {
			if (other.littleName != null)
				return false;
		} else if (!littleName.equals(other.littleName))
			return false;
		return true;
	}

	public String getBigName() {
		return bigName;
	}

	public String getLittleName() {
		return littleName;
	}

	public String getDescription() {
		return description;
	}

	public boolean isObligatory() {
		return isObligatory;
	}

	public boolean canBeAnonymous() {
		return canBeAnonymous;
	}

}
