# SantaBot

Projet de création d'un bot discord permettant la réalisation d'un secret santa.
Il utilise Maven pour le build et javacord pour communiquer avec l'API officiel Discord

# Utilisation

Le projet étant publique, vous pouvez le cloner et le build par vous même.

Pour une utilisation plus pratique et indépendante, vous pouvez utiliser l'image docker

```docker run -d registry.gitlab.com/hugobriatte/botsanta TOKEN DB_FILE_LOCATION```

Cela vous permettera de ne pas a voir de gerer de dépendances et de tourner sans machine virtuelle hormis java.