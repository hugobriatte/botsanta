package hugoBriatte.santabot.serverManagment.dataManagment.exception;

/**
 * Exception threw when a user is allready present
 * @author hugo
 *
 */
public class AllreadyPresentException extends DataQueryException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2439005838900689834L;

	public AllreadyPresentException() {
		super();
	}

	public AllreadyPresentException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

	public AllreadyPresentException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public AllreadyPresentException(String arg0) {
		super(arg0);
	}

	public AllreadyPresentException(Throwable arg0) {
		super(arg0);
	}

	
}
