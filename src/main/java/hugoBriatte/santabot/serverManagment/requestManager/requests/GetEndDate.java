package hugoBriatte.santabot.serverManagment.requestManager.requests;

import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.event.message.MessageCreateEvent;

import hugoBriatte.santabot.serverManagment.dataManagment.DataQuery;
import hugoBriatte.santabot.serverManagment.dataManagment.exception.DataQueryException;

public class GetEndDate extends Request {
	
	protected DataQuery query;

	public GetEndDate(DataQuery query) {
		super("Retourne la date de fin du secret santa de ce serveur");
		this.query = query;
	}

	@Override
	public void execute(String argumentList, MessageCreateEvent event) {
		MessageBuilder builder = new MessageBuilder();
		try {
			builder.append("Ce secret santa se termine le : " + query.getEndDate(event.getServer().get()));
		} catch (DataQueryException e) {
			builder.append(BDDERROR);
		}
		builder.send(event.getChannel());
	}

}
