package hugoBriatte.santabot.serverManagment.requestManager.requests;

import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.event.message.MessageCreateEvent;

import hugoBriatte.santabot.serverManagment.dataManagment.DataQuery;
import hugoBriatte.santabot.serverManagment.dataManagment.exception.DataQueryException;

public class IAmInRequest extends Request {

	protected DataQuery query;

	public IAmInRequest(DataQuery query) {
		super("Permet de verifier si quelqu'un est dans les listes");
		this.query = query;
	}

	@Override
	public void execute(String argumentList, MessageCreateEvent event) {
		MessageBuilder response = new MessageBuilder();
		try {
			if(query.isParticipating(event.getMessage().getAuthor().asUser().get(), event.getServer().get())) {
				response.append("Tu particies au secret santa :3");
			} else {
				response.append("Tu participe poa au secret santa :x");
			}
		} catch (DataQueryException e) {
			response.append(BDDERROR);
		}
		response.send(event.getChannel());
	}

}
