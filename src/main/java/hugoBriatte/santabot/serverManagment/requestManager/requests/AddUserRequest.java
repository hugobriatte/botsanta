package hugoBriatte.santabot.serverManagment.requestManager.requests;

import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;

import hugoBriatte.santabot.serverManagment.ServerManager;
import hugoBriatte.santabot.serverManagment.dataManagment.DataQuery;
import hugoBriatte.santabot.serverManagment.dataManagment.exception.AllreadyPresentException;
import hugoBriatte.santabot.serverManagment.dataManagment.exception.DataQueryException;

public class AddUserRequest extends Request {

	protected DataQuery query;
	
	public AddUserRequest(DataQuery query) {
		super("Ajoute la personne éxécutant la requête aux participants");
		this.query = query;
	}

	@Override
	public void execute(String argumentList, MessageCreateEvent event) {
		MessageBuilder response = new MessageBuilder();
		try {
			try {
				query.addServer(event.getServer().get());
			} catch (AllreadyPresentException e) {
				
			}
			
			try {
				User user = event.getMessage().getAuthor().asUser().get();
				query.addUser(user,
						event.getServer().get());
				response.append("Vous avez été ajouté :3333");
			} catch (AllreadyPresentException e) {
				response.append("Vous etes déjà la :3");
			}
		} catch (DataQueryException e) {
			response.append(BDDERROR);
		}
		response.send(event.getChannel());
	}

}
