package hugoBriatte.santabot.serverManagment.dataManagment.exception;

/**
 * Exception throw when The date couldn't have been written/checked
 * @author hugo
 *
 */
public class DataQueryException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5200704808597565115L;

	public DataQueryException() {
		super();
	}

	public DataQueryException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

	public DataQueryException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public DataQueryException(String arg0) {
		super(arg0);
	}

	public DataQueryException(Throwable arg0) {
		super(arg0);
	}

	
}
