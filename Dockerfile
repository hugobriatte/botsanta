#CONSTRUCTION
FROM maven:3.3.9-jdk-8 AS builder
COPY src/ /tmp/src/
COPY pom.xml /tmp/
WORKDIR /tmp/
RUN mvn compile

#PACKAGE
FROM builder AS packager
RUN mvn clean compile dependency:copy-dependencies
WORKDIR ./target/

#PREPARATION DU RUNNER
FROM openjdk:8 AS runner
WORKDIR /release/
COPY --from=packager /tmp/target/dependency/* ./dependency/
COPY --from=packager /tmp/target/classes/hugoBriatte ./classes/hugoBriatte/
WORKDIR ./classes/
CMD ["DEFAULT_TOKEN", "DEFAULT_FILE"]
ENTRYPOINT ["java", "-classpath", "../dependency/*:.", "hugoBriatte.santabot.App"]
