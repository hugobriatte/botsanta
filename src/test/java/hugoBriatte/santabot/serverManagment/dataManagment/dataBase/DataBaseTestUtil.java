package hugoBriatte.santabot.serverManagment.dataManagment.dataBase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.h2.tools.Server;

/**
 * Test utility to simulate a server for tests
 * @author hugo
 *
 */
public class DataBaseTestUtil {
	
	public static String URL;
	public static String USER = "sa";
	public static String PASSWD = "";
	private static Server serv;
	
	static {
		try {
			serv = Server.createTcpServer(new String[] {}).start();
			URL = "jdbc:h2:mem:testing";
			Class.forName("org.h2.Driver");
			Connection con = getConnection();
			con.prepareStatement("DROP ALL OBJECTS DELETE FILES").executeUpdate();
			con.close();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Return a connection to the server
	 * @return The connexion
	 * @throws SQLException
	 */
	public static Connection getConnection() throws SQLException {
		return DriverManager.getConnection(URL, USER, PASSWD);
	}
	
	/**
	 * Close the server
	 */
	public static void close() {
		serv.stop();
	}
}
