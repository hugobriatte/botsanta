package hugoBriatte.santabot.serverManagment.requestManager.requests.arguments;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class ArgumentsMap<E> extends HashMap<ArgumentDescriptor, E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5242355570204441637L;

	public ArgumentsMap() {
		super();
	}

	public ArgumentsMap(int arg0, float arg1) {
		super(arg0, arg1);
	}

	public ArgumentsMap(int arg0) {
		super(arg0);
	}

	public ArgumentsMap(Map<? extends ArgumentDescriptor, ? extends E> arg0) {
		super(arg0);
	}

	public Entry<ArgumentDescriptor, E> getEntry(boolean isLittleKey, String key) {
		for(Entry<ArgumentDescriptor, E> entry : this.entrySet()) {
			if((isLittleKey && entry.getKey().littleName.equals(key))
			 || (!isLittleKey && entry.getKey().bigName.equals(key))) return entry;
		}
		return null;
	}
	
	public boolean isDefined(boolean isLittleKey, String key) {
		return getEntry(isLittleKey, key) != null;
	}

	
}
