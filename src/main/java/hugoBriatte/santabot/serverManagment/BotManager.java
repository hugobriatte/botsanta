package hugoBriatte.santabot.serverManagment;

import java.util.HashMap;
import java.util.Map;

import org.javacord.api.DiscordApi;
import org.javacord.api.entity.channel.ServerTextChannel;
import org.javacord.api.entity.server.Server;

import hugoBriatte.santabot.serverManagment.dataManagment.DataQuery;
import hugoBriatte.santabot.serverManagment.dataManagment.exception.AllreadyPresentException;
import hugoBriatte.santabot.serverManagment.dataManagment.exception.DataQueryException;
import hugoBriatte.santabot.serverManagment.requestManager.requests.AddUserRequest;
import hugoBriatte.santabot.serverManagment.requestManager.requests.EchoRequest;
import hugoBriatte.santabot.serverManagment.requestManager.requests.GetAllParticipantsRequest;
import hugoBriatte.santabot.serverManagment.requestManager.requests.GetEndDate;
import hugoBriatte.santabot.serverManagment.requestManager.requests.HelpRequest;
import hugoBriatte.santabot.serverManagment.requestManager.requests.IAmInRequest;
import hugoBriatte.santabot.serverManagment.requestManager.requests.LaunchSantaRequest;
import hugoBriatte.santabot.serverManagment.requestManager.requests.RemoveRequest;
import hugoBriatte.santabot.serverManagment.requestManager.requests.Request;
import hugoBriatte.santabot.serverManagment.requestManager.requests.SetDefaultChannel;
import hugoBriatte.santabot.serverManagment.requestManager.requests.SetEndDateRequest;
import hugoBriatte.santabot.serverManagment.requestManager.requests.StopSantaRequest;

public class BotManager {
	
	protected DataQuery query;
	protected Map<Server, ServerManager> servManagers = new HashMap<Server, ServerManager>();
	protected Map<String, Request> requests = new HashMap<String, Request>();
	public static final String pREFIX = "s!";
	public static final String wELCOME_MESSAGE = "Bienvenue, je suis SecretSanta, le bot santa pour faire des secrets santa.\n"
			+ "Je tiens a prevenir que j'utilise une base de donnée associé a celui qui fait tourner le bot. "
			+ "Je ne fais que stocker les id des participants sous formes de nombres pour garder en mémoire qui participe et qui offre des cadeaux a qui UwU.\n"
			+ "Merki et bon période de décembre :3";
	
	public BotManager(DiscordApi api, DataQuery dataQuery) {
		this.query = dataQuery;
		
		for(Server serv : api.getServers()) {
			servManagers.put(serv, new ServerManager(serv));
			try {
				query.addServer(serv);
			} catch (DataQueryException e) {
				/* RAS */
			}
		}
		
		requests.put("echo", new EchoRequest());
		requests.put("add", new AddUserRequest(query));
		requests.put("check", new IAmInRequest(query));
		requests.put("remove", new RemoveRequest(query));
		requests.put("launch", new LaunchSantaRequest(query, servManagers));
		requests.put("getEndDate", new GetEndDate(query));
		requests.put("setEndDate", new SetEndDateRequest(query));
		requests.put("getParticipants", new GetAllParticipantsRequest(query));
		requests.put("setChannel", new SetDefaultChannel(servManagers));
		requests.put("stop", new StopSantaRequest(query));
		requests.put("help", new HelpRequest(requests));
		
		api.addServerJoinListener(e -> {
			Server serv = e.getServer();
			servManagers.put(serv, new ServerManager(serv));
			ServerTextChannel channel = servManagers.get(serv).getDefaultChannel();
			try {
				query.addServer(e.getServer());
				channel.sendMessage(wELCOME_MESSAGE);
			} catch (AllreadyPresentException e1) {
				channel.sendMessage("Petit problème coté BDD, le serv est déjà présent");
			} catch (DataQueryException e1) {
				channel.sendMessage("On a eu un problème durant l'entrée du serv :/");
			}
		});
		
		api.addServerLeaveListener(e -> {
			try {
				query.removeServer(e.getServer());
			} catch (DataQueryException e1) {
				e1.printStackTrace();
			}
		});
		
		api.addMessageCreateListener(e -> {
			String[] tab = e.getMessage().getContent().split("\\s+", 2);
			String argList = (tab.length >= 2 ? tab[1] : "");
			try {
				if(tab[0].startsWith(pREFIX)) requests.get(tab[0].replace(pREFIX, "")).execute(argList, e);
			} catch(NullPointerException ee) {
				/* RAS */
			}
		});
	}

}
