package hugoBriatte.santabot.serverManagment.requestManager.requests.arguments.dataType;

import java.util.InputMismatchException;

import org.javacord.api.entity.server.Server;

/**
 * Represent a dataType asked by the bot
 * @author Hugo
 *
 * @param <E> The type that translate to
 */
public interface DataType<E> {

	/**
	 * Check if a String is correct (can be translated)
	 * @param value The value
	 * @return true if it can, false otherwise
	 */
	public default boolean isCorrect(Server serv, String value) {
		try {
			translate(serv, value);
			return true;
		} catch (InputMismatchException e) {
			return false;
		}
	}
	
	/**
	 * Translate a String to its value
	 * @param input The String
	 * @return The object
	 * @throws InputMismatchException if it couldn't been translated
	 */
	public E translate(Server serv, String input) throws InputMismatchException;
	
}
